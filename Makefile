# .PHONY предназначен для указания в качестве цели 'выполнение команды', а не 'создание файла'
# .DEFAULT_GOAL указывает на дефолтную команду, в данном примере 'make init'

.DEFAULT_GOAL: init

.PHONY: init
init: build up status

.PHONY: build
build:
	docker-compose build

.PHONY: up
up:
	docker-compose up -d

.PHONY: down
down:
	docker-compose down

.PHONY: status
status:
	docker-compose ps

.PHONY: tinker
tinker:
	docker-compose exec -u 1000:1000 php sh